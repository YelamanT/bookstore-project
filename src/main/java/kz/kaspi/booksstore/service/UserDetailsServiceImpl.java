package kz.kaspi.booksstore.service;

import kz.kaspi.booksstore.entity.UserEntity;
import kz.kaspi.booksstore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByName(username);

        if (userEntity == null)
            throw new UsernameNotFoundException("User " + username + " not found");

        ArrayList<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();

        for (String role : userEntity.getRoles().split(",")) {
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(role));
        }

        

        return new User(userEntity.getName(), userEntity.getPassword(), simpleGrantedAuthorities);
    }
}
