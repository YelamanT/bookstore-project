package kz.kaspi.booksstore.service;

import kz.kaspi.booksstore.entity.Book;
import kz.kaspi.booksstore.entity.UserEntity;
import kz.kaspi.booksstore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService   {



    @Autowired
    private UserRepository repository;


    public void save (UserEntity user) {
        repository.save(user);
    }

    public Boolean contains (String name) {
        return repository.findByName(name) != null;
    }

    public UserEntity findByName () {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return repository.findByName(username);
    }

    public List<Book> cartBooks () {
        return findByName().getBookList();
    }

    public void addBook (Book book) {
        UserEntity user = findByName();
        user.getBookList().add(book);
        repository.save(user);

    }
    public void buyBook (Book book) {
        UserEntity user = findByName();
        user.getBookList().remove(book);
        user.getBoughtBooks().add(book);
        repository.save(user);

    }
    public void deleteBook (Book book) {
        UserEntity user = findByName();
        user.getBookList().remove(book);
        repository.save(user);

    }

    public void checkoutBooks () {
        UserEntity user = findByName();
        user.getBoughtBooks().addAll(user.getBookList());
        user.getBookList().clear();
        repository.save(user);
    }

    public List <Book> orderHistory () {
        return findByName().getBoughtBooks();
    }
}
