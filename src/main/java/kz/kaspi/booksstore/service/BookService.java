package kz.kaspi.booksstore.service;

import kz.kaspi.booksstore.entity.Book;
import kz.kaspi.booksstore.repository.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BooksRepository repository;

    public List<Book> findAll () { return repository.findAll();}

    public List<Book> findAllGreaterThanZero () { return repository.findAllByQuantityIsGreaterThan(0);}

    public Optional<Book> findById (Long id) {return  repository.findById(id);}

    public List<Book> findByTitle (String title) {
        return repository.findByTitleLikeIgnoreCase(title);
    }

    public void saveOrUpdate (Book book) {
        repository.save(book);
    }

    public void buyBook( Long id) {
        Book book = repository.getOne(id);
        book.setQuantity(book.getQuantity()-1);
        repository.save(book);
    }


    public Set<String> allSections() {

        List <String> list=new ArrayList<>();
        for (Book e:repository.findAll()) {
            list.addAll(Arrays.asList(e.getSection().split(","))
                    .stream()
                    .map(h->h.toUpperCase().trim())
                    .collect(Collectors.toList())
            );
        }
        Set<String> sections=new HashSet<String>(list);
        return sections;
    }


    public void checkout (List<Book> list) {
        for (Book b: list) {
            buyBook(b.getBook_id());
        }
    }


    public Set <String> languages () {

        Set <String> languageSet=new HashSet<>();

        for (Book e: repository.findAll()) {
            languageSet.add(e.getLanguage().toUpperCase());
        }
        return languageSet;
    }

    public List<Book> filteredBooks (String section, String language, int amountFrom, int amountTo) {
        if (language.isBlank()) {
            language="%%";
        }
        section="%"+section+"%";

        return repository.findFiltered(section,language,amountFrom,amountTo);
    }
}
