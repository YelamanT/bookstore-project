package kz.kaspi.booksstore.controller;


import kz.kaspi.booksstore.DTO.BookFilter;
import kz.kaspi.booksstore.entity.Book;
import kz.kaspi.booksstore.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping ("/admin")
public class AdminController {

    private final BookService service;

    public AdminController(BookService service) {
        this.service = service;
    }

    private void modelAdder (Model model) {
        model.addAttribute("sectionList",service.allSections());
        model.addAttribute("languageList", service.languages());
        model.addAttribute("filterObject", new BookFilter());
    }

    @GetMapping
    public String adminPage(Model model) {

        model.addAttribute("list", service.findAll());
        modelAdder(model);
        return "admin-page";

    }

    @GetMapping ("/update/{id}")
    public String updateBook (@PathVariable Long id, Model model) {
        Book book =service.findById(id).orElse(null);

        if (book==null) {
            model.addAttribute("message", "Book is not found");
            return "Error-Page";
        }

        model.addAttribute("Book", book);
        return "update-or-add-books";
    }

    @GetMapping("/new")
    public String addNewBook (Model model) {
        model.addAttribute("Book", new Book());
        return "update-or-add-books";
    }

    @PostMapping
    public String saveOrUpdate(Book book,Model model) {
        if (book.getTitle().isBlank() || book.getAuthor().isBlank()
                || book.getLanguage().isBlank() || book.getSection().isBlank()) {

            model.addAttribute("message","Fields can not consist of only whitespaces");
            return "Error-Page";
        }

        service.saveOrUpdate(book);
        model.addAttribute("list",service.findAll());
        modelAdder(model);
        return "admin-page";
    }

    @GetMapping("/search")
    public String search(Model model, @RequestParam(defaultValue = "") String title) {

        if (title.isBlank()) {
            model.addAttribute("message","Search has failed, please try again");
            return "Error-page";
        }

        title=title+"%";

        List<Book> list=service.findByTitle(title);
        model.addAttribute("list",list);
        modelAdder(model);
        return "admin-page";
    }

    @GetMapping("/filter")
    public String filter (BookFilter filterObject, Model model) {

        model.addAttribute("list", service.filteredBooks(filterObject.getSection(),
                filterObject.getLanguage(),filterObject.getAmountFrom(),filterObject.getAmountTo()));

        modelAdder(model);
        return "admin-page";
    }


}
