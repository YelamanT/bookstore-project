package kz.kaspi.booksstore.controller;


import kz.kaspi.booksstore.DTO.BookFilter;
import kz.kaspi.booksstore.entity.Book;
import kz.kaspi.booksstore.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@Controller
@RequestMapping ("/books")
public class BookController {

    private final BookService service;


    public BookController(BookService service) {
        this.service = service;
    }

    private void modelAdder (Model model) {
        model.addAttribute("sectionList",service.allSections());
        model.addAttribute("languageList", service.languages());
        model.addAttribute("filterObject", new BookFilter());
    }


    @GetMapping
    public String allBooks (Model model) {
        model.addAttribute("list", service.findAllGreaterThanZero());
        modelAdder(model);
        return "home-page";
    }


    @GetMapping("/search")
    public String search(Model model, @RequestParam(defaultValue = "") String title) {

        if (title.isBlank()) {
            model.addAttribute("message","Search has failed, please try again");
            return "Error-page";
        }

        title=title+"%";

        List<Book> list=service.findByTitle(title);
        model.addAttribute("list",list);
        modelAdder(model);
        return "home-page";
    }

    @GetMapping("/filter")
    public String filter (BookFilter filterObject,Model model) {

        model.addAttribute("list", service.filteredBooks(filterObject.getSection(),
                filterObject.getLanguage(),filterObject.getAmountFrom(),filterObject.getAmountTo()));

        modelAdder(model);

        return "home-page";
    }





}
