package kz.kaspi.booksstore.controller;


import kz.kaspi.booksstore.entity.Book;
import kz.kaspi.booksstore.service.BookService;
import kz.kaspi.booksstore.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cart")
public class CartController {


    private final BookService service;
    private final UserService userService;



    public CartController(BookService service, UserService userService) {
        this.service = service;
        this.userService=userService;
    }

    @GetMapping
    public String cart(Model model) {
        model.addAttribute("list", userService.cartBooks());
        return "basket";

    }


    @GetMapping("/buy/{id}")
    public String buyBook(@PathVariable Long id, Model model) {

        Book book = service.findById(id).orElse(null);

        if (book==null) {
            model.addAttribute("message", "Book is not found");
            return "Error-Page";
        }
        if (book.getQuantity()==0) {
            model.addAttribute("message","This book is sold out, " +
                    "we are sorry for inconvenience");
            return "Error-Page";
        }

        service.buyBook(id);
        userService.buyBook(book);

        return "redirect:/cart" ;

    }

    @GetMapping ("/basket/{id}")
    public String basket(@PathVariable Long id, Model model) {
        Book book = service.findById(id).orElse(null);


        if (book==null) {
            model.addAttribute("message", "Book is not found");
            return "Error-Page";
        }
        if (book.getQuantity()==0) {
            model.addAttribute("message","This book is sold out, " +
                    "we are sorry for inconvenience");
            return "Error-Page";
        }

        userService.addBook(book);

        return "redirect:/books";
    }

    @GetMapping ("/checkout")
    public String checkout(Model model) {
        if (userService.cartBooks().isEmpty()) {
            model.addAttribute("message","nothing to checkout");
            return "Error-Page";
        }

        service.checkout(userService.cartBooks());
        userService.checkoutBooks();
        return "redirect:/books";
    }

    @GetMapping("/history")
    public String orderHistory (Model model) {
        model.addAttribute("list",userService.orderHistory());
        return "order-history";
    }

    @GetMapping ("/delete/{id}")
    public String deleteBook (@PathVariable Long id, Model model) {
        Book book = service.findById(id).orElse(null);

        if (book==null) {
            model.addAttribute("message", "Book is not found");
            return "Error-Page";
        }


        userService.deleteBook(book);
        return "redirect:/cart";
    }

}
