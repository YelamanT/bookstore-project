package kz.kaspi.booksstore.controller;



import kz.kaspi.booksstore.entity.UserEntity;
import kz.kaspi.booksstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/signup")
public class RegistrationController {



    @Autowired
    private UserService service;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping
    public String signup(String passwordClone,UserEntity user, Model model) {

        if (service.contains(user.getName())) {
            model.addAttribute("userNameError", true);
            user.setName("");
            user.setPassword("");
            model.addAttribute("user",user);
            return "registration-page";
        }else if (!(passwordClone.equals(user.getPassword()))) {
            model.addAttribute("passwordError",true);
            user.setPassword("");
            model.addAttribute("user", user);
            return "registration-page";
        }

        user.setPassword(
                passwordEncoder.encode(user.getPassword())
        );

        user.setRoles("ROLE_USER");

        service.save(user);

        return "login";
    }

    @GetMapping("/registry")
    public String registryUser(Model model) {
        model.addAttribute("user",new UserEntity());
        return "registration-page";
    }
}
