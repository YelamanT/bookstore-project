package kz.kaspi.booksstore.DTO;

public class BookFilter {

    private String section;
    private String language;
    private int amountFrom;
    private int amountTo;

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getAmountFrom() {
        return amountFrom;
    }

    public void setAmountFrom(int amountFrom) {
        this.amountFrom = amountFrom;
    }

    public int getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(int amountTo) {
        this.amountTo = amountTo;
    }

    @Override
    public String toString() {
        return "BookFilter{" +
                "section='" + section + '\'' +
                ", language='" + language + '\'' +
                ", amountFrom=" + amountFrom +
                ", amountTo=" + amountTo +
                '}';
    }
}
