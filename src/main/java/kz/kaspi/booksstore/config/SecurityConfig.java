package kz.kaspi.booksstore.config;


import kz.kaspi.booksstore.service.UserDetailsServiceImpl;


import org.springframework.context.annotation.Bean;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/books").permitAll()
                .antMatchers("/books/search/**").permitAll()
                .antMatchers("/books/filter/**").permitAll()
                .antMatchers("/books/**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/signup/**").permitAll()
                .antMatchers("/cart/**").hasAnyRole("USER", "ADMIN")
                .anyRequest().authenticated()
                .and()
                    .formLogin().permitAll()
                    .loginPage("/login")
                    .failureUrl("/login-error")
                    .defaultSuccessUrl("/books")

                .and()
                    .logout()
                    .logoutSuccessUrl("/books")
                    .deleteCookies("JSESSIONID");


    }


    @Bean
    public PasswordEncoder passwordEncoder () {
        return new BCryptPasswordEncoder();
    }


}
