package kz.kaspi.booksstore.repository;

import kz.kaspi.booksstore.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksRepository extends JpaRepository <Book, Long>{


    List<Book> findByTitleLikeIgnoreCase(String title);

    List<Book> findAllByQuantityIsGreaterThan (int number);


    @Query ("SELECT b FROM Book b WHERE " +
            "(:section IS NULL OR  LOWER(b.section) LIKE LOWER(:section))" +
            "AND (:language IS NULL OR LOWER(b.language) LIKE LOWER(:language))" +
            "AND (:amountFrom IS NULL OR b.price >= :amountFrom) " +
            "AND (:amountTo IS NULL OR b.price <= :amountTo)")
    List<Book> findFiltered (
        @Param("section") String section,
        @Param("language") String language,
        @Param ("amountFrom") int amountFrom,
        @Param("amountTo") int amountTo
    );

}
