package kz.kaspi.booksstore.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Book {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long book_id;

    private String title;

    private String author;

    private int price;

    private String section;

    private String language;

    private int quantity;

    @ManyToMany(mappedBy = "bookList")
    private List<UserEntity> userList = new ArrayList<>();

    @ManyToMany(mappedBy = "boughtBooks")
    private List<UserEntity> soldBooks = new ArrayList<>();



    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<UserEntity> getUserList() {
        return userList;
    }

    public void setUserList(List<UserEntity> userList) {
        this.userList = userList;
    }

    public List<UserEntity> getSoldBooks() {
        return soldBooks;
    }

    public void setSoldBooks(List<UserEntity> soldBooks) {
        this.soldBooks = soldBooks;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + book_id +
                ", name='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", section='" + section + '\'' +
                ", language='" + language + '\'' +
                ", quantity=" + quantity +
                '}';
    }



    @Override
    public boolean equals(Object comparedObject) {
        if (this == comparedObject) {
            return true;
        }

        if (!(comparedObject instanceof Book)) {
            return false;
        }

        Book comparedBook = (Book) comparedObject;

        return this.book_id.equals(comparedBook.book_id);
    }
}
