create table users
(
    user_id       bigserial primary key,
    name     varchar not null UNIQUE,
    password varchar not null,
    roles    varchar not null
);

create table cart
(
    order_id BIGSERIAL,
    book_id BIGINT references book (book_id),
    user_id BIGINT references users (user_id),
    PRIMARY KEY (order_id)

);

create table completed_orders
(
    order_id BIGSERIAL,
    book_id BIGINT references book (book_id),
    user_id BIGINT references users (user_id),
    PRIMARY KEY (order_id)

);