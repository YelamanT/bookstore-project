CREATE TABLE BOOK (
                      book_id  BIGSERIAL Primary key,
                      title varchar (35),
                      author varchar (35),
                      price int,
                      section varchar (35),
                      language varchar (35),
                      quantity int
)